import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.junit.Assert.assertTrue;

public class FirstTests {
    WebDriver driver;
    HomePage homePage;
    Actions action;

    @Before
    public void before(){
        driver = WebDriverFactory.getDriver("ff");
        homePage = new HomePage(driver);
        action = new Actions(driver);
        driver.manage().window().maximize();
        driver.get("https://allo.ua/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void fistCheckPopUp(){
        homePage.getAccountButton().click();
        assertTrue(homePage.getLoginPopup().isDisplayed());
    }

    @Test
    public void loginTest(){
        homePage.getAccountButton().click();
        homePage.getEmail_input().sendKeys("0930960787");
        homePage.getPassword_input().sendKeys("qwerty123");
        homePage.getSubmit_button().isEnabled();
        homePage.getSubmit_button().click();
        driver.navigate().refresh();
        homePage.getAccount_name().isEnabled();
    }

    @Test(timeout = 1000)
    public void locationTest(){
        assertTrue(homePage.getCity_tooltip().isDisplayed());
        homePage.getConfirm_city_tooltip_button().click();
        homePage.getClose_notification_popup().click();
        assertTrue(driver.findElement(By.xpath(format(homePage.LOCATION_CITY,"Харків"))).isDisplayed());
        homePage.getCity_button().click();
        assertTrue(homePage.getCity_list_popup().isDisplayed());
        driver.findElement(By.xpath(format(homePage.ITEM_CITY,"Львів"))).click();
        assertTrue(driver.findElement(By.xpath(format(homePage.LOCATION_CITY,"Львів"))).isDisplayed());
    }

    @Test @Ignore
    public void leftSideMenuTest(){
        homePage.getConfirm_city_tooltip_button().click();
        homePage.getClose_notification_popup().click();
        WebElement target = driver.findElement(By.xpath(format(homePage.LEFT_SIDE_MENU_ITEM,"Смартфони та телефони")));
        action.moveToElement(target).perform();
        driver.findElement(By.xpath(format(homePage.MENU_ITEM_ID,"menu_mobile_phone"))).isDisplayed();
        WebElement targetTv = driver.findElement(By.xpath(format(homePage.LEFT_SIDE_MENU_ITEM,"Телевізори, аудіо та фото")));
        action.moveToElement(targetTv).perform();
        driver.findElement(By.xpath(format(homePage.MENU_ITEM_ID,"menu_tv"))).isDisplayed();
        WebElement targetAk = driver.findElement(By.xpath(format(homePage.LEFT_SIDE_MENU_ITEM,"Навушники і акустика")));
        action.moveToElement(targetAk).perform();
        assertTrue(driver.findElement(By.xpath(format(homePage.MENU_ITEM_ID,"menu_naushniki_i_akustika"))).isDisplayed());
    }

    @Test @Disabled
    public void purchaseBucketTest(){
        homePage.getConfirm_city_tooltip_button().click();
        homePage.getClose_notification_popup().click();
        homePage.getMain_search_input().sendKeys("Samsung A50");
        homePage.getButton().click();
        WebElement item = homePage.getFirst_element();
        action.moveToElement(item).perform();
        homePage.getBuy_button().click();
        assertTrue(homePage.getOrder_popup().isDisplayed());
        homePage.getOrder_button().click();
        homePage.getClose_notification_popup().click();
        assertTrue(homePage.getOrder_table().isDisplayed());
        homePage.getLogo().click();
        assertTrue(driver.findElement(By.xpath(format(homePage.PURCHASE_BUCKET,"1"))).isDisplayed());
    }

    @After
    public void after(){
        driver.close();
    }
}
